/*
 * Go REST API
 *
 * API version: 1.0.0
 */

package api

import (
	"github.com/rs/zerolog/log"
	"github.com/tidwall/buntdb"
)

// Run starts an API router
func Run() {
	// configure the logger
	LogConfigure()

	// read configuration from config.yaml
	err := config.Read()
	if err != nil {
		log.Fatal().Err(err).Msg("")
	}

	// open database
	log.Debug().Msgf("opening database %s", config.DBFile)
	db := new(buntdb.DB)
	db, err = buntdb.Open(config.DBFile)
	if err != nil {
		log.Fatal().Err(err).Msg("")
	}
	defer func() {
		if err = db.Close(); err != nil {
			log.Fatal().Err(err).Msg("")
		}
	}()

	// shrink the database
	log.Debug().Msgf("shrinking database %s", config.DBFile)
	err = db.Shrink()
	if err != nil {
		log.Fatal().Err(err).Msg("")
	}

	// Configure routers and start listener
	switch config.TLSEnabled {
	case true:
		HTTPTLSListener(HTTPRouters(db))
	default:
		HTTPListener(HTTPRouters(db))
	}
}
