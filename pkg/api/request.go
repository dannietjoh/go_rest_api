/*
 * Go REST API
 *
 * API version: 1.0.0
 */

package api

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
)

// JSONRequestReader handles requests with json body.
// Checks headers and the JSON-encoded body, decodes and stores the result
// in the value pointed to by data, then validates its content.
// Returns the request paramters, the http status code and the error message
func JSONRequestReader(r *http.Request, data interface{}) (requestParams RequestParams, status int, err error) {
	// get request parameters
	requestParams.URL = mux.Vars(r)
	requestParams.Query = r.URL.Query()

	// Check Content-Type header
	validContentType := strings.Contains(r.Header.Get("Content-Type"), "application/json")
	if !validContentType {
		return requestParams, http.StatusBadRequest, fmt.Errorf("Invalid Content Type")
	}

	// Read request body
	requestBody, err := ioutil.ReadAll(io.LimitReader(r.Body, config.RequestLimit))
	if err != nil {
		return requestParams, http.StatusBadRequest, err
	}

	// Close reader
	err = r.Body.Close()
	if err != nil {
		return requestParams, http.StatusBadRequest, err
	}

	// Check for empty body
	if requestBody == nil {
		return requestParams, http.StatusBadRequest, fmt.Errorf("Empty Request")
	}

	// decode json body
	err = json.Unmarshal(requestBody, &data)
	if err != nil {
		return requestParams, http.StatusBadRequest, err
	}

	// validate data
	err = validate().Struct(data)
	if err != nil {
		return requestParams, http.StatusBadRequest, err
	}

	return
}

// RequestReader - handles requests
func RequestReader(r *http.Request) (requestParams RequestParams, status int, err error) {
	// get request parameters
	requestParams.URL = mux.Vars(r)
	requestParams.Query = r.URL.Query()

	return requestParams, http.StatusOK, nil
}
