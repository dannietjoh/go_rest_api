/*
 * Go REST API
 *
 * API version: 1.0.0
 */

package api

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"github.com/tidwall/buntdb"
)

// Index - serves a static index page
func Index(db *buntdb.DB) HTTPAdapter {
	return func(handler http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			// handle response
			ResponseWriter(w, "go-rest-api", http.StatusOK, nil)
			return
		})
	}
}

// ReadExamples is a HTTPAdapter that reads all examples
func ReadExamples(db *buntdb.DB) HTTPAdapter {
	return func(handler http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			// vars
			var examples []Example
			var result Result

			// handle request
			_, status, err := RequestReader(r)
			if err != nil {
				ResponseWriter(w, result, status, err)
				return
			}

			// read examples from database, unmarshal and add to []Example
			examples = []Example{}
			err = db.View(func(tx *buntdb.Tx) error {
				err := tx.Ascend("", func(key, value string) bool {
					if !strings.Contains(key, "_buildstatus") {
						var example Example
						err = json.Unmarshal([]byte(value), &example)
						if err != nil {
							ResponseWriter(w, result, http.StatusInternalServerError, err)
							return false
						}
						examples = append(examples, example)
					}
					return true
				})
				return err
			})
			if err != nil {
				ResponseWriter(w, result, http.StatusInternalServerError, err)
				return
			}

			// encode object to json
			body, err := json.Marshal(examples)
			if err != nil {
				ResponseWriter(w, result, http.StatusInternalServerError, err)
				return
			}

			// handle response
			ResponseWriter(w, body, http.StatusOK, err)
		})
	}
}

// CreateExample is a HTTPAdapter that creates an example
func CreateExample(db *buntdb.DB) HTTPAdapter {
	return func(handler http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			// vars
			var example Example
			var result Result

			// handle request
			_, status, err := JSONRequestReader(r, &example)
			if err != nil {
				ResponseWriter(w, result, status, err)
				return
			}

			// check if example exists
			err = example.Read(example.Name, db)
			if err == nil {
				ResponseWriter(w, result, http.StatusBadRequest, fmt.Errorf("%s already exists", example.Name))
				return
			}

			// marshal object to json and write to database
			err = example.Write(example.Name, db)
			if err != nil {
				ResponseWriter(w, result, http.StatusInternalServerError, err)
				return
			}

			// handle response
			ResponseWriter(w, result, http.StatusOK, err)
		})
	}
}

// ReadExample is a HTTPAdapter that reads an example
func ReadExample(db *buntdb.DB) HTTPAdapter {
	return func(handler http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			// vars
			var example Example
			var result Result

			// handle request
			requestParams, status, err := RequestReader(r)
			if err != nil {
				ResponseWriter(w, result, status, err)
				return
			}

			// read object from database and unmarshal
			err = example.Read(requestParams.URL["example"], db)
			if err != nil {
				if err == buntdb.ErrNotFound {
					status = http.StatusNotFound
					err = fmt.Errorf("%s %s", requestParams.URL["example"], buntdb.ErrNotFound)
				} else {
					status = http.StatusInternalServerError
				}
				ResponseWriter(w, result, status, err)
				return
			}

			// encode object to json
			body, err := json.Marshal(example)
			if err != nil {
				ResponseWriter(w, result, http.StatusInternalServerError, err)
				return
			}

			// handle response
			ResponseWriter(w, body, http.StatusOK, err)
		})
	}
}

// UpdateExample is a HTTPAdapter that updates an example
func UpdateExample(db *buntdb.DB) HTTPAdapter {
	return func(handler http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			// vars
			var example Example
			var result Result

			// handle request
			requestParams, status, err := JSONRequestReader(r, &example)
			if err != nil {
				ResponseWriter(w, result, status, err)
				return
			}

			// fail if new example name differs from old example name
			if requestParams.URL["example"] != example.Name {
				ResponseWriter(w, result, http.StatusBadRequest, fmt.Errorf("not allowed to update example name"))
				return
			}

			// read object from database and unmarshal
			err = example.Read(requestParams.URL["example"], db)
			if err != nil {
				if err == buntdb.ErrNotFound {
					status = http.StatusNotFound
					err = fmt.Errorf("%s %s", requestParams.URL["example"], buntdb.ErrNotFound)
				} else {
					status = http.StatusInternalServerError
				}
				ResponseWriter(w, result, status, err)
				return
			}

			// delete object from database
			err = example.Delete(requestParams.URL["example"], db)
			if err != nil {
				if err == buntdb.ErrNotFound {
					status = http.StatusNotFound
					err = fmt.Errorf("%s %s", requestParams.URL["example"], buntdb.ErrNotFound)
				} else {
					status = http.StatusInternalServerError
				}
				ResponseWriter(w, result, status, err)
				return
			}

			// marshal object to json and write to database
			err = example.Write(example.Name, db)
			if err != nil {
				ResponseWriter(w, result, http.StatusInternalServerError, err)
				return
			}

			// handle response
			ResponseWriter(w, result, http.StatusOK, err)
		})
	}
}

// DeleteExample is a HTTPAdapter that deletes an example
func DeleteExample(db *buntdb.DB) HTTPAdapter {
	return func(handler http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			// vars
			var example Example
			var result Result

			// handle request
			requestParams, status, err := RequestReader(r)
			if err != nil {
				ResponseWriter(w, result, status, err)
				return
			}

			// check if example exists
			err = example.Read(requestParams.URL["example"], db)
			if err != nil {
				ResponseWriter(w, result, http.StatusNotFound, fmt.Errorf("%s %s", requestParams.URL["example"], buntdb.ErrNotFound))
				return
			}

			// delete object from database
			err = example.Delete(requestParams.URL["example"], db)
			if err != nil {
				if err == buntdb.ErrNotFound {
					status = http.StatusNotFound
					err = fmt.Errorf("%s %s", requestParams.URL["example"], buntdb.ErrNotFound)
				} else {
					status = http.StatusInternalServerError
				}
				ResponseWriter(w, result, status, err)
				return
			}

			// handle response
			ResponseWriter(w, result, http.StatusOK, err)
		})
	}
}

// ReadHealth is a HTTPAdapter that reads server health
func ReadHealth(db *buntdb.DB) HTTPAdapter {
	return func(handler http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			// vars
			var health Health
			var result Result

			// handle request
			_, status, err := RequestReader(r)
			if err != nil {
				ResponseWriter(w, result, status, err)
				return
			}

			// todo: gather useful health metrics

			// handle response
			ResponseWriter(w, health, http.StatusOK, err)
		})
	}
}
