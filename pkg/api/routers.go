/*
 * Go REST API
 *
 * API version: 1.0.0
 */

package api

import (
	"crypto/tls"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
	"github.com/rs/zerolog/log"
	"github.com/tidwall/buntdb"
)

// Route type
type Route struct {
	AuthMethod  string
	AuthLevel   string
	Name        string
	Method      string
	Pattern     string
	HandlerFunc func(db *buntdb.DB) HTTPAdapter
}

// Routes type
type Routes []Route

var routes = Routes{
	{
		"none",
		"none",
		"Index",
		"GET",
		"",
		Index,
	},

	{
		"apiKey",
		"admin",
		"ReadExamples",
		"GET",
		"api",
		ReadExamples,
	},

	{
		"apiKey",
		"admin",
		"CreateExample",
		"POST",
		"api",
		CreateExample,
	},

	{
		"apiKey",
		"group",
		"ReadExample",
		"GET",
		"api/{example}",
		ReadExample,
	},

	{
		"apiKey",
		"group",
		"UpdateExample",
		"PUT",
		"api/{example}",
		UpdateExample,
	},

	{
		"apiKey",
		"admin",
		"DeleteExample",
		"DELETE",
		"api/{example}",
		DeleteExample,
	},

	{
		"none",
		"none",
		"ReadHealth",
		"GET",
		"health",
		ReadHealth,
	},
}

// HTTPAdapter defines the http adapter type
type HTTPAdapter func(http.Handler) http.Handler

// HTTPAdapterWrapper wraps a sequence of adapters into a http handler
//
// Note that the sequence is handled in reverse
func HTTPAdapterWrapper(adapters ...HTTPAdapter) http.Handler {
	var handler http.Handler
	for _, adapter := range adapters {
		handler = adapter(handler)
	}
	return handler
}

// HTTPRouters configures the http routers
func HTTPRouters(db *buntdb.DB) (router *mux.Router) {
	// Configure HTTP routes
	router = mux.NewRouter().StrictSlash(true)
	for _, route := range routes {
		var handler http.Handler
		var HandlerAdapter = route.HandlerFunc
		handler = HTTPAdapterWrapper(
			HandlerAdapter(db),
			AuthAdapter(route.AuthMethod, route.AuthLevel),
			LogAdapter(route.Name),
		)

		router.
			Methods(strings.ToUpper(route.Method)).
			Path(config.BasePath + route.Pattern).
			Name(route.Name).
			Handler(handler)
	}

	return
}

// HTTPListener starts a HTTP listener
func HTTPListener(handler http.Handler) {
	// HTTP Server configuration
	httpServer := &http.Server{
		Addr:    config.ListenAddress + ":" + config.ListenPort,
		Handler: handler,
	}

	// Start Plain listener
	log.Info().Msgf("starting %s server on port %s", config.ServerName, config.ListenPort)
	err := httpServer.ListenAndServe()
	if err != nil {
		log.Fatal().Err(err).Msg("")
	}
}

// HTTPTLSListener starts a HTTP TLS listener
func HTTPTLSListener(handler http.Handler) {
	// Determine configured TLS version
	var TLSMinVersion uint16
	switch config.TLSMinVersion {
	case "tls11":
		TLSMinVersion = tls.VersionTLS11
	case "tls12":
		TLSMinVersion = tls.VersionTLS12
	}

	// Supported TLS cipher suites
	var TLSCipherSuites = map[string]uint16{
		"TLS_RSA_WITH_AES_128_CBC_SHA":            0x002f,
		"TLS_RSA_WITH_AES_256_CBC_SHA":            0x0035,
		"TLS_RSA_WITH_AES_128_CBC_SHA256":         0x003c,
		"TLS_RSA_WITH_AES_128_GCM_SHA256":         0x009c,
		"TLS_RSA_WITH_AES_256_GCM_SHA384":         0x009d,
		"TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA":    0xc009,
		"TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA":    0xc00a,
		"TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA":      0xc013,
		"TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA":      0xc014,
		"TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256": 0xc023,
		"TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256":   0xc027,
		"TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256":   0xc02f,
		"TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256": 0xc02b,
		"TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384":   0xc030,
		"TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384": 0xc02c,
		"TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305":    0xcca8,
		"TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305":  0xcca9,
	}

	// Enabled TLS cipher suites
	var CipherSuites []uint16
	for _, cipher := range config.TLSCipherSuites {
		CipherSuites = append(CipherSuites, TLSCipherSuites[cipher])
	}

	// TLS configuration
	tlsConfig := &tls.Config{
		MinVersion:               TLSMinVersion,
		PreferServerCipherSuites: config.TLSPreferServerCipherSuites,
		CipherSuites:             CipherSuites,
	}

	// HTTP Server configuration
	httpServer := &http.Server{
		Addr:      config.ListenAddress + ":" + config.ListenPort,
		Handler:   handler,
		TLSConfig: tlsConfig,
	}

	// Configure http version 1.1 or 2
	switch strings.ToLower(config.HTTPVersion) {
	case "http1.1":
		httpServer.TLSNextProto = make(map[string]func(*http.Server, *tls.Conn, http.Handler), 0)
	case "http2":
		// default
	default:
		log.Fatal().Msg("unsupported http version")
	}

	// Start TLS listener
	log.Info().Msgf("starting %s %s server with %s on port %s", config.ServerName, config.HTTPVersion, config.TLSMinVersion, config.ListenPort)
	err := httpServer.ListenAndServeTLS(config.TLSPublicKeyFile, config.TLSPrivateKeyFile)
	if err != nil {
		log.Fatal().Err(err).Msg("")
	}
}
