/*
 * Go REST API
 *
 * API version: 1.0.0
 */

package api

import (
	"encoding/json"

	"github.com/rs/zerolog/log"
	"github.com/tidwall/buntdb"
)

// Example data
type Example struct {
	Name           string                   `json:"name" validate:"required"`
	Version        string                   `json:"version" validate:"required"`
	NestedExamples map[string]NestedExample `json:"nestedexamples,omitempty" validate:"dive,required"`
}

// Read reads Example from a database and json unmashals it into e
func (e *Example) Read(object string, db *buntdb.DB) (err error) {
	err = db.View(func(tx *buntdb.Tx) error {
		object, err = tx.Get(object)
		return err
	})
	if err != nil {
		return
	}

	err = json.Unmarshal([]byte(object), &e)

	log.Debug().Msgf("Read from DB: %+v", *e)
	return
}

// WriteJSON json marshals Example and writes it to a database
func (e *Example) Write(object string, db *buntdb.DB) (err error) {
	enc, err := json.Marshal(&e)
	if err != nil {
		return
	}

	err = db.Update(func(tx *buntdb.Tx) error {
		_, _, err = tx.Set(object, string(enc), nil)
		return err
	})

	log.Debug().Msgf("Write to DB: %+v", *e)
	return
}

// Delete deletes a Example from a database
func (e *Example) Delete(object string, db *buntdb.DB) (err error) {
	db.Update(func(tx *buntdb.Tx) error {
		_, err := tx.Delete(object)
		return err
	})

	log.Debug().Msgf("Delete from DB: %+v", *e)
	return
}

// NestedExample data
type NestedExample struct {
	Name       string `json:"name" validate:"required"`
	RandomData string `json:"randomdata" validate:"required"`
}

// Result status data
type Result struct {
	Status int    `json:"status" validate:"required"`
	Result string `json:"result" validate:"required"`
}

// Health data
type Health struct {
	Status  string `json:"status" validate:"required"`
	Message string `json:"message" validate:"required"`
}

// RequestParams contains http request URL and query parameters
type RequestParams struct {
	URL   map[string]string
	Query map[string][]string
}
