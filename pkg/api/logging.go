/*
 * Go REST API
 *
 * API version: 1.0.0
 */

package api

import (
	"flag"
	"net/http"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

// LogConfigure configures the application log level
func LogConfigure() {
	// logging: set unix time format
	zerolog.TimeFieldFormat = ""

	debug := flag.Bool("debug", false, "sets log level to debug")
	info := flag.Bool("info", false, "sets log level to info")
	warn := flag.Bool("warn", false, "sets log level to warn")
	error := flag.Bool("error", false, "sets log level to error")

	flag.Parse()

	// Configure log level, default is Info
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	if *debug {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)

		log.Debug().Msg("log level: debug")
	}
	if *info {
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
		log.Info().Msg("log level: info")
	}
	if *warn {
		zerolog.SetGlobalLevel(zerolog.WarnLevel)
		log.Warn().Msg("log level: warn")
	}
	if *error {
		zerolog.SetGlobalLevel(zerolog.ErrorLevel)
		log.Error().Msg("log level: error")
	}

}

// LogAdapter logs http requests in debug mode
func LogAdapter(name string) HTTPAdapter {
	return func(handler http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			start := time.Now()

			log.Debug().Msgf(
				"%s %s %s %s",
				r.Method,
				r.RequestURI,
				name,
				time.Since(start),
			)

			handler.ServeHTTP(w, r)
		})
	}
}
