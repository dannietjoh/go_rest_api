/*
 * Go REST API
 *
 * API version: 1.0.0
 */

package api

import (
	"io/ioutil"

	"github.com/rs/zerolog/log"
	yaml "gopkg.in/yaml.v2"
)

// Config contains application configuration
type Config struct {
	AUTHKeyFile                 string                 `yaml:"auth_key_file" validate:"required"`
	ServerName                  string                 `yaml:"server_name" validate:"required"`
	ListenAddress               string                 `yaml:"listen_address" validate:"-"`
	ListenPort                  string                 `yaml:"listen_port" validate:"required"`
	BasePath                    string                 `yaml:"base_path" validate:"required"`
	RequestLimit                int64                  `yaml:"request_limit" validate:"required"`
	QueueWorkers                int                    `yaml:"queue_workers" validate:"required"`
	QueueSize                   int                    `yaml:"queue_size" validate:"required"`
	DBFile                      string                 `yaml:"db_file" validate:"required"`
	HTTPVersion                 string                 `yaml:"http_version" validate:"required"`
	TLSEnabled                  bool                   `yaml:"tls_enabled" validate:"-"`
	TLSMinVersion               string                 `yaml:"tls_minversion" validate:"-"`
	TLSPreferServerCipherSuites bool                   `yaml:"tls_prefer_server_cipher_suites" validate:"-"`
	TLSCipherSuites             []string               `yaml:"tls_cipher_suites" validate:"-"`
	TLSPrivateKeyFile           string                 `yaml:"tls_private_key_file" validate:"-"`
	TLSPublicKeyFile            string                 `yaml:"tls_public_key_file" validate:"-"`
}

// ConfigFile contains file parameters
type ConfigFile struct {
	FileName   string `yaml:"file_name" validate:"-"`
	Template   string `yaml:"template" validate:"-"`
	SourcePath string `yaml:"source_path" validate:"-"`
	DestPath   string `yaml:"dest_path" validate:"-"`
}

// config makes the configuration available globally
var config Config

// Read implements the read method for Config
func (c *Config) Read() (err error) {
	data, err := ioutil.ReadFile("config/config.yaml")
	if err != nil {
		return
	}
	err = yaml.Unmarshal(data, c)
	if err != nil {
		return
	}

	// validate data
	err = validate().Struct(c)

	log.Debug().Msgf("configuration: %+v", *c)
	return
}
