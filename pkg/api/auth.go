/*
 * Go REST API
 *
 * API version: 1.0.0
 */

package api

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
	"github.com/rs/zerolog/log"
	yaml "gopkg.in/yaml.v2"
)

// AuthKeys -
type AuthKeys map[string]string

// Read implements the read method for Keys
func (k *AuthKeys) Read() (err error) {
	data, err := ioutil.ReadFile(config.AUTHKeyFile)
	if err != nil {
		return err
	}

	err = yaml.Unmarshal(data, k)
	return err
}

// AuthAdapter handles authentication
func AuthAdapter(AuthMethod string, AuthLevel string) HTTPAdapter {
	return func(handler http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			var authkeys AuthKeys
			var result Result

			// read api acces keys from config.AUTHKeyFile
			err := authkeys.Read()
			if err != nil {
				log.Warn().Err(err).Msgf("unable to read keys from %s", config.AUTHKeyFile)
			}

			// get request parameters
			requestParams := mux.Vars(r)

			// handle auth methods
			switch strings.ToLower(AuthMethod) {
			case "none":
				handler.ServeHTTP(w, r)
				return
			case "apikey":
				// handle access levels
				switch strings.ToLower(AuthLevel) {
				case "admin":
					switch r.Header.Get("X-Api-Key") {
					case "":
						ResponseWriter(w, result, http.StatusUnauthorized, fmt.Errorf("Unauthorized, no API key set"))
						return
					case authkeys["adminkey"]:
						handler.ServeHTTP(w, r)
						return
					default:
						ResponseWriter(w, result, http.StatusUnauthorized, fmt.Errorf("Unauthorized, invalid API key"))
						return
					}
				case "group":
					switch r.Header.Get("X-Api-Key") {
					case "":
						ResponseWriter(w, result, http.StatusUnauthorized, fmt.Errorf("Unauthorized, no API key set"))
						return
					case authkeys["adminkey"]:
						handler.ServeHTTP(w, r)
						return
					case authkeys[requestParams["group"]]:
						handler.ServeHTTP(w, r)
						return
					default:
						ResponseWriter(w, result, http.StatusUnauthorized, fmt.Errorf("Unauthorized, invalid API key"))
						return
					}
				default:
					ResponseWriter(w, result, http.StatusUnauthorized, fmt.Errorf("Unauthorized"))
					return
				}
			default:
				ResponseWriter(w, result, http.StatusUnauthorized, fmt.Errorf("Unauthorized"))
				return
			}
		})
	}
}
