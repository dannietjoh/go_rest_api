/*
 * Go REST API
 *
 * API version: 1.0.0
 */

package api

import (
	"encoding/json"
	"net/http"

	"github.com/rs/zerolog/log"
)

// ResponseData constructs http response data based on responseType
func ResponseData(responseData interface{}, statusData int, errData error) (body []byte, headers map[string]string, status int, err error) {
	var jsonHeader = "application/json; charset=UTF-8"
	var textHeader = "text/html; charset=UTF-8"
	var marshalErr error
	headers = make(map[string]string)
	status = statusData

	switch data := responseData.(type) {
	case Result:
		data.Status = statusData
		if errData == nil {
			data.Result = "Success"
		} else {
			data.Result = errData.Error()
		}
		body, marshalErr = json.Marshal(data)
		headers["Content-Type"] = jsonHeader
	case Health:
		// todo : implement health checks
		health := "healthy"
		if health == "healthy" {
			data.Status = "OK"
			data.Message = "Healthy"
		} else {
			data.Status = "NOTOK"
			data.Message = "Unhealthy"
		}
		body, marshalErr = json.Marshal(data)
		headers["Content-Type"] = jsonHeader
	case []byte:
		body = data
		headers["Content-Type"] = jsonHeader
	case string:
		body = []byte(data)
		headers["Content-Type"] = textHeader
	default:
		body = nil
		headers = nil
	}

	if marshalErr != nil {
		return nil, nil, http.StatusInternalServerError, err
	}

	return body, headers, status, nil
}

// ResponseWriter writes responses based on responseData, http status and error and logs errors in debug mode
func ResponseWriter(w http.ResponseWriter, responseData interface{}, status int, err error) {
	// Log errors
	if err != nil {
		log.Debug().Err(err).Msg("")
	}

	// Generate response
	body, headers, status, err := ResponseData(responseData, status, err)
	if err != nil {
		log.Debug().Err(err).Msg("could not generate response")
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(nil)
	}

	// set headers
	for key, value := range headers {
		w.Header().Set(key, value)
	}
	w.WriteHeader(status)

	// write response
	_, err = w.Write(body)
	if err != nil {
		log.Debug().Err(err).Msg("could not write response")
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(nil)
	}

}
