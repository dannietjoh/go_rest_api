.DEFAULT_GOAL := help
SHELL := /bin/bash

BUILD_NAME := go-rest-api
BUILD_TAG := $(shell git describe --always --tags --abbrev=0)
DOCKER_REGISTRY :=
GIT_SERVER := gitlab.com
GIT_GROUP := dannietjoh
GIT_REPO := go-rest-api
GOOS := linux
OPENSHIFT_MASTER := https://
OS := $(shell uname)

ifeq ($(OS), Darwin)
	BROWSER_OPEN := open
else
ifeq ($(OS), Linux)
	BROWSER_OPEN := xdg-open
else
ifeq ($(OS), Windows_NT)
	BROWSER_OPEN := explorer
endif
endif
endif

##@ Build tasks
.PHONY: dep
dep: ## Update package dependencies
	@dep ensure

.PHONY: test-unit
test-unit: ## Unit tests
	@docker run --rm \
		--volume ${PWD}:/go/src/${GIT_SERVER}/${GIT_GROUP}/${GIT_REPO} \
		--workdir /go/src/${GIT_SERVER}/${GIT_GROUP}/${GIT_REPO} \
		${DOCKER_REGISTRY}golang:latest \
		go test -v .

.PHONY: test-functional
test-functional: ## Functional tests
	@docker run --rm \
		--link go-rest-api \
		--volume ${PWD}/test/postman:/etc/newman \
		--tty ${DOCKER_REGISTRY}newman:alpine \
		run postman_collection.json \
		--environment=postman_environment_docker.json \
		--insecure

.PHONY: lint
lint: ## Run linters
	@docker run --rm \
		--volume ${PWD}/Dockerfile:/Dockerfile:ro \
		${DOCKER_REGISTRY}dockerlint -p
	@docker run --rm \
		--volume ${PWD}:/go/src/${GIT_SERVER}/${GIT_GROUP}/${GIT_REPO} \
		--workdir /go/src/${GIT_SERVER}/${GIT_GROUP}/${GIT_REPO}/api \
		${DOCKER_REGISTRY}metalinter:latest \
		./... --skip=spec --vendor --deadline=120s --cyclo-over=20 --line-length=120 --concurrency=4

.PHONY: build
build: ## Build
	@CGO_ENABLED=0 GOOS=${GOOS} go build -v

.PHONY: build-docker
build-docker: ## Build docker container
	@docker build . --tag ${BUILD_NAME}

.PHONY: client-stub
client-stub: ## Build client stub
	@sudo rm -rf spec/models/client
	@export GO_POST_PROCESS_FILE="/usr/local/bin/gofmt -w"
	@docker run --rm \
		--volume ${PWD}:/local openapitools/openapi-generator-cli \
		generate \
		--input-spec /local/spec/openapi.yml \
		--generator-name go \
		--output /local/spec/models/client/

.PHONY: server-stub
server-stub: ## Build server stub
	@sudo rm -rf spec/models/server
	@export GO_POST_PROCESS_FILE="/usr/local/bin/gofmt -w"
	@docker run --rm \
		--volume ${PWD}:/local openapitools/openapi-generator-cli \
		generate \
		--input-spec /local/spec/openapi.yml \
		--generator-name go-server \
		--output /local/spec/models/server/

.PHONY: docker-server-stub
docker-server-stub: ## Build docker container with server stub
	@docker build --network=host . --tag ${BUILD_NAME}-stub

.PHONY: tag
tag: ## Tag containers
	@docker tag ${BUILD_NAME}:latest ${BUILD_NAME}:${BUILD_TAG}
	@docker tag ${BUILD_NAME}:latest ${DOCKER_REGISTRY}${BUILD_NAME}:${BUILD_TAG}
	@docker tag ${BUILD_NAME}:latest ${DOCKER_REGISTRY}${BUILD_NAME}:latest

.PHONY: push
push: ## Push containers to registry
	@docker login -u ${DOCKER_REPO_USER} -p ${DOCKER_REPO_TOKEN} https://${DOCKER_REGISTRY}
	@docker push ${DOCKER_REGISTRY}${BUILD_NAME}:${BUILD_TAG}
	@docker push ${DOCKER_REGISTRY}${BUILD_NAME}:latest

.PHONY: push-nologin
push-nologin: ## Push containers to registry
	@docker push ${DOCKER_REGISTRY}${BUILD_NAME}:${BUILD_TAG}
	@docker push ${DOCKER_REGISTRY}${BUILD_NAME}:latest

.PHONY: verify-local
verify-local: ## verify succesful deployment locally
	@curl localhost:8080/v1/health

.PHONY: clean-docker
clean-docker: ## Clean exited docker containers and untagged images
	@docker rm $(shell docker ps --quiet --all --no-trunc --filter "status=exited") || true
	@docker rmi $(shell docker images --filter "dangling=true" -q) || true

.PHONY: all
all: ## Full test lint build-docker deploy cycle
	@${MAKE} destroy-local dep test lint build-docker deploy-local test-functional destroy-local

##@ Service management
.PHONY: deploy-local
deploy-local: ## Deploy to localhost
	@docker run \
		--rm \
		--detach \
		--privileged \
		--name docker-builder \
		docker:dind
	@docker run \
		--rm \
		--link docker-builder \
		--env DOCKER_HOST='tcp://docker-builder:2375' \
		--detach \
		--name go-rest-api \
		--publish 8080:8080 \
		${BUILD_NAME}:latest -debug

# TODO
.PHONY: deploy-openshift
deploy-openshift: ## Deploy to openshift
	exit 0; oc login --token ${ROBOT_TOKEN} ${OPENSHIFT_MASTER}
	exit 0; oc rollout latest ${BUILD_NAME}

destroy-local: ## Deploy to localhost
	@docker rm -f docker-builder || true
	@docker rm -f go-rest-api || true

.PHONY: run
run: ## go run
	@go run main.go

.PHONY: up
up: ## Bring docker containers up
	@docker-compose up --detach

.PHONY: down
down: ## Bring docker containers down
	@docker-compose down

.PHONY: downup
downup: ## Restart docker containers
	@${MAKE} down up

.PHONY: obliterate
obliterate: ## Destroy docker containers and volumes
	@docker-compose down --volumes

.PHONY: restart
restart: ## Restart docker containers
	@docker-compose restart

.PHONY: pull
pull: ## Pull docker containers
	@docker-compose pull

.PHONY: logs
logs: ## Show docker container logs
	@docker-compose logs --follow

.PHONY: top
top: ## Show docker container processes
	@docker-compose top

##@ Services
.PHONY: browse-server
browse-server: ## Open server in browser
	@$(BROWSER_OPEN) $(API_URL)

.PHONY: ssh
ssh:
	@ssh $(CI_HOST)

##@ Helpers
.PHONY: cloc
cloc: ## Show Lines of Code analysis
	@cloc --vcs git --quiet

.PHONY: goconvey
goconvey: ## run goconvey tool
	goconvey -cover -excludedDirs "vendor,spec" -port 8010 &

.PHONY: doc
doc: ## run go documentation tool
	@killall godoc
	@godoc -http :8020 &
	@$(BROWSER_OPEN) http://localhost:8020

.PHONY: openapi-editor
openapi-editor: ## run openapi
	@docker rm -f swagger-editor || true
	@docker run --detach --name swagger-editor --publish 8021:8080 swaggerapi/swagger-editor
	@$(BROWSER_OPEN) http://localhost:8021

.PHONY: help
help: ## Display this help
	awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)
